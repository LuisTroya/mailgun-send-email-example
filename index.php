<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;

$mg = new Mailgun('your-key');
$domain = 'your domain';
$mg->sendMessage($domain,
    array(
        'from' => 'bob@example.com',
        'to' => 'bob@example.com',
        'cc' => 'bob@example.com',
        'bcc' => 'bob@example.com',
        'subject' => 'The PHP SDK is awesome!',
        'text' => 'Aqui van los textos normales',
        'html' => '<h1>Pero aqui van los HTMLs</h1>'
    ),
    array(
        'attachment' =>
            array(
                'filePath' => '@C:\xampp\htdocs\mailgn\files\prg2.jpg' // Full Path
            )
    ));
